
import axios from 'axios';

export default post = (endpoint, postData, callback) => {
    
    axios.defaults.headers.post['Content-Type'] = 'application/json'
    axios.post(endpoint, postData).then(response => {

        console.log(`API_ENDPOINT: ${endpoint}`);
        console.log(`API_DATA: ${JSON.stringify(postData, null, 4)}`);
        console.log(`API_RESPONSE: ${JSON.stringify(response, null, 4)}`);
        
        if (response.data.code === "101") {
            callback(response);
        }

    }).catch(error => {
        console.log(`API_ERROR: ${error}`);
    });
}

export const get = (endpoint, callback) => {
    axios.get(endpoint).then(response => {

        console.log(`API_ENDPOINT: ${endpoint}`);
        console.log(`API_RESPONSE: ${JSON.stringify(response, null, 4)}`);

        callback(response.data);
    }).catch(error => {
        console.log(`API_ERROR: ${error}`);
    });
};